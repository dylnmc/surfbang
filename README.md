# surf!bang


An easy solution for browsing man pages in ViM.

## Installation

```
sudo make install
```

## Usage

### Command Line

The purpose of this project is to seemlessly replace the old man pager with vim

``` bash
$ man {ManPage}
```

### Vim

* You will now always have access to the `:Man` command. For example: `:Man {ManPage}`
  - This can understand sections. For example: `:Man 3 printf`

* <kbd>Shift</kbd>+<kbd>K</kbd> will parse the text under the cursor and open the man page if it exists.
  - This is able to understand sections
  - For example pressing <kbd>Shift</kbd>+<kbd>K</kbd> on `printf(3)` will open
    `3 printf`

* If the screen size changes, you can re-flow the text with
  <kbd>Ctrl</kbd>+<kbd>R</kbd>

## Licence

This project is licensed under the GPL License. See the [LICENSE.md](LICENSE.md) file for details.

## TODO

1. Don't use `$VIMRUNTIME/ftplugin/man.vim` because it's terrible
  - This means creating own `:Man` command and filling in all necessary bugs
